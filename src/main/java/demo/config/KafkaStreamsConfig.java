package demo.config;

import demo.Application;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.ValueMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.EnableKafkaStreams;
import org.springframework.kafka.annotation.KafkaStreamsDefaultConfiguration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaStreamsConfiguration;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.apache.kafka.streams.StreamsConfig.APPLICATION_ID_CONFIG;


@Configuration
@EnableKafka
@EnableKafkaStreams
public class KafkaStreamsConfig {


    private static final Logger LOG = LoggerFactory.getLogger(Application.class);

    private KafkaProperties kafkaProperties;

    @Autowired
    public KafkaStreamsConfig(KafkaProperties kafkaProperties){
        this.kafkaProperties=kafkaProperties;
    }


    @Bean(name = KafkaStreamsDefaultConfiguration.DEFAULT_STREAMS_CONFIG_BEAN_NAME)
    public KafkaStreamsConfiguration kStreamsConfig() {

        Map<String, Object> configProps = kafkaProperties.buildStreamsProperties();

        configProps.put(org.apache.kafka.streams.StreamsConfig.APPLICATION_ID_CONFIG, "streams-app");
        //configProps.put(org.apache.kafka.streams.StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        configProps.put(org.apache.kafka.streams.StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        configProps.put(org.apache.kafka.streams.StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());

        return new KafkaStreamsConfiguration(configProps);
    }


    @Bean
    public KTable<String, Long> kTableCountLower(StreamsBuilder streamsBuilder) {

        Serde<String> STRING_SERDE = Serdes.String();

        KStream<String, String> kStream = streamsBuilder
            .stream("stream-input-topic", Consumed.with(STRING_SERDE, STRING_SERDE));

        KTable<String, Long> wordCounts = kStream
            .mapValues((ValueMapper<String, String>) String::toLowerCase)
            .flatMapValues(value -> Arrays.asList(value.split("\\W+")))
            .groupBy((key, word) -> word, Grouped.with(STRING_SERDE, STRING_SERDE))
            .count();

        return wordCounts;
    }

    @Bean
    public Topology buildPipelineKTableLower(StreamsBuilder streamsBuilder, KTable<String, Long> kTable) {

        kTable.toStream()
                  .foreach((word, count) -> LOG.info("kTableLower: " + word + " -> " + count));

        /*
        Topology topology = streamsBuilder.build();
        KafkaStreams streams = new KafkaStreams(topology, streamsConfiguration);
        streams.start();
        */

        //kTable.toStream().to("stream-output-topic");

        return streamsBuilder.build();

    }


    @Bean
    public Topology buildPipelineKTableUpper(StreamsBuilder streamsBuilder) {

        Serde<String> STRING_SERDE = Serdes.String();

        KStream<String, String> kStream = streamsBuilder
            .stream("stream-input-topic", Consumed.with(STRING_SERDE, STRING_SERDE));


        KTable<String, Long> wordCounts = kStream
            //.mapValues((ValueMapper<String, String>) String::toLowerCase)
            .flatMapValues(value -> Arrays.asList(value.split("\\W+")))
            .groupBy((key, word) -> word, Grouped.with(STRING_SERDE, STRING_SERDE))
            .count();

        wordCounts.toStream()
                  .foreach((word, count) -> LOG.info("KTable Upper: " + word + " -> " + count));

        /*
        Topology topology = streamsBuilder.build();
        KafkaStreams streams = new KafkaStreams(topology, streamsConfiguration);
        streams.start();
        */

        //wordCounts.toStream().to("stream-output-topic");

        return streamsBuilder.build();

    }



    @Bean
    public Topology buildPipelineStreamsFilter(StreamsBuilder streamsBuilder) {

        Serde<String> STRING_SERDE = Serdes.String();

        KStream<String, String> kStream = streamsBuilder
            .stream("stream-input-topic", Consumed.with(STRING_SERDE, STRING_SERDE));

        KStream<String, String> kStreamOperation =
            kStream
                .map((key, value) -> KeyValue.pair(value, value.toUpperCase()))
                .filter((key, value) -> value.contains("A"));

        kStreamOperation
                  .foreach((msg, dummy) -> LOG.info("Streams FILTER -> " + msg));

        kStreamOperation.to("stream-output-topic-FILTER-A");

        return streamsBuilder.build();

    }

    @Bean
    public Topology buildPipelineConsumerFilterA(StreamsBuilder streamsBuilder) {

        Serde<String> STRING_SERDE = Serdes.String();

        KStream<String, String> kStream = streamsBuilder
            .stream("stream-output-topic-FILTER-A", Consumed.with(STRING_SERDE, STRING_SERDE));


        kStream
            .foreach((msg, dummy) -> LOG.info("CONSUMER FILTER A -> " + msg));


        return streamsBuilder.build();

    }







}
