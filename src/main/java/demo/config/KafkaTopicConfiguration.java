package demo.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.common.config.TopicConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
@EnableKafka
public class KafkaTopicConfiguration {

    @Bean
    public NewTopic topicInput() {
      return TopicBuilder.name("stream-input-topic")
                         .partitions(6)
                         .replicas(1)
                         //.config(TopicConfig.COMPRESSION_TYPE_CONFIG, "stream-input-topic")
                         .build();
    }

    @Bean
    public NewTopic topicOutPut() {
      return TopicBuilder.name("stream-output-topic-FILTER-A")
                       .partitions(6)
                       .replicas(1)
                       //.config(TopicConfig.COMPRESSION_TYPE_CONFIG, "stream-input-topic")
                       .build();
  }


}
