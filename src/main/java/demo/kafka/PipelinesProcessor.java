package demo.kafka;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.ValueMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class PipelinesProcessor {

/*
  private static final Serde<String> STRING_SERDE = Serdes.String();

  @Autowired
  void buildPipeline(StreamsBuilder streamsBuilder) {

    KStream<String, String> messageStream = streamsBuilder
        .stream("stream-input-topic", Consumed.with(STRING_SERDE, STRING_SERDE));

    KTable<String, Long> wordCounts = messageStream
        .mapValues((ValueMapper<String, String>) String::toLowerCase)
        .flatMapValues(value -> Arrays.asList(value.split("\\W+")))
        .groupBy((key, word) -> word, Grouped.with(STRING_SERDE, STRING_SERDE))
        .count();

    wordCounts.toStream()
              .foreach((word, count) -> System.out.println("word: " + word + " -> " + count));


    wordCounts.toStream().to("stream-output-topic");



  }
*/


}
