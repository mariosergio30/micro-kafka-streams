package demo.webapi;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.kstream.KTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.config.StreamsBuilderFactoryBean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import demo.Application;


@RestController
@RequestMapping("/")  
public class Controler {

	private static final Logger LOG = LoggerFactory.getLogger(Application.class);


	@Autowired
	private StreamsBuilderFactoryBean streamBuilder;

	@Autowired
	private KTable<String, Long> kTable;



	@RequestMapping(value="/test-streams", method = RequestMethod.GET)
	public ResponseEntity<String> testStreams() {

			try {

		   	//KafkaStreams kafkaStreams = streamBuilder.getKafkaStreams();

				kTable.toStream()
							.foreach((word, count) -> LOG.info("RESPONSE: " + word + " -> " + count));

				//kafkaStreams.start();

				//Thread.sleep(30000);
				//kafkaStreams.close();


				/*
			ReadOnlyKeyValueStore<String, Long> counts = kafkaStreams.. .store(
					StoreQueryParameters.fromNameAndType("counts", QueryableStoreTypes.keyValueStore())
			);

			 */

			LOG.info("DEMO: PRODUCER CONTROLER");


		} catch (Exception e) {
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),HttpStatus.INTERNAL_SERVER_ERROR);
		}

		// SUCCESS
		return new ResponseEntity<String>(HttpStatus.OK.getReasonPhrase(),HttpStatus.OK);
	}




}



