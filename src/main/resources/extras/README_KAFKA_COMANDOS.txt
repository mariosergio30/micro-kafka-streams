


zookeeper-server-start.bat .\config\zookeeper.properties

-- EXECUTE 3 brokers
kafka-server-start.bat .\config\server0.properties
kafka-server-start.bat .\config\server1.properties
kafka-server-start.bat .\config\server2.properties


kafka-acls.bat --bootstrap-server localhost:9090 --version


kafka-topics.bat --bootstrap-server localhost:9090  --create --topic mario
kafka-topics.bat  --bootstrap-server localhost:9090 --create --topic test --replication-factor 1 --partitions 1


kafka-topics.bat --bootstrap-server localhost:9090  --delete--topic mario

kafka-topics.bat --bootstrap-server localhost:9090 --list 
kafka-topics.bat --bootstrap-server localhost:9090 --describe
kafka-topics.bat --bootstrap-server localhost:9090 --describe --topic order_requested



-- simples producer
kafka-console-producer.bat --broker-list localhost:9090 --topic order_requested

-- producer with key
kafka-console-producer.bat --broker-list localhost:9090 --topic order_requested --property parse.key=true --property key.separator=:
{"code": "0100200","name": "Guitar OAZ","especification": null,"category": "MUSIC","salePrice": 1025.32,"supplier": null}


-- consumer instance without a group, will only read messages produced after consumer run (to see all, we use --from-beginning
kafka-console-consumer.bat --bootstrap-server localhost:9090 --topic product-new --from-beginning


kafka-console-consumer.bat --bootstrap-server localhost:9090 --topic order_requested

-- consumer instances in a group, will read all messages not read yet by others instances in the same group (since this group have already created)
kafka-console-consumer.bat --bootstrap-server localhost:9090 --topic order_requested --group sales
kafka-console-consumer.bat --bootstrap-server localhost:9090 --topic order_requested --group spy


kafka-consumer-groups.bat --bootstrap-server localhost:9090 --list


kafka-consumer-groups.bat --bootstrap-server localhost:9090 --describe --group sales
kafka-consumer-groups.bat --bootstrap-server localhost:9090 --describe --group spy


{"code": "0100200","name": "Guitar OAZ","especification": null,"category": "MUSIC","salePrice": 1025.32,"supplier": null}


ADVANCED
-----------------

-- INCREASE/DECREASE OFFSETS
kafka-consumer-groups.bat --bootstrap-server localhost:9090 --topic product-new --group espiaoPT --reset-offsets --shift-by 6 --execute



CONFLUENT: 
OBS1: COLOCAR my_confluent_key.properties no config do dir do kafka
OBS2: VIA JAVA TESTEI COM O KAFDROP, mas não tive sucesso


kafka-console-producer.bat --broker-list pkc-ymrq7.us-east-2.aws.confluent.cloud:9092  --producer.config ../../config/my_confluent_key.properties --topic topic_teste

kafka-console-consumer.bat --bootstrap-server pkc-ymrq7.us-east-2.aws.confluent.cloud:9092  --consumer.config ../../config/my_confluent_key.properties --topic topic_teste --group sales

kafka-console-consumer.bat --bootstrap-server pkc-ymrq7.us-east-2.aws.confluent.cloud:9092  --consumer.config ../../config/my_confluent_key.properties --topic product-new --group espiaoPT


